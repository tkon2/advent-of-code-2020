import re
import itertools

data = []
with open('puzzle_input') as file:
    for line in file:
        data.append(int(line.replace('\n', '')))


data = sorted(data)


old = 0
distances = [None, 0, 0, 0]
for item in data:
    distances[item - old] += 1
    old = item

# Add final adapter
distances[3] += 1

print(distances[1:])
print(distances[1] * distances[3])
