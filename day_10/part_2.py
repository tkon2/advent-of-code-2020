import re
import itertools

data = []
with open('puzzle_input') as file:
    for line in file:
        data.append(int(line.replace('\n', '')))

goal = max(data) + 3

# Add the final adapter
data.append(goal)

# Add the starting value
data.append(0)

data = sorted(data)


def getPossibles(num):
    ans = []
    for item in data:
        if (item <= num):
            continue
        if (item > num + 3):
            return ans
        ans.append(item)
    return []


timelines = [[0]]

count = 0
i = 0
while(len(timelines)):
    i += 1
    if (i % 100000 == 0):
        print("Timelines: %s" % len(timelines))
        print("Last count: %s" % len(timelines[-1]))
        print("Goals: %s" % count)
    current = timelines.pop()
    lastItem = current[-1]

    if (lastItem == goal):
        count += 1
        continue
    # lastIndex = data.index(lastItem)
    possibles = getPossibles(lastItem)
    # print("Possibles: %s" % possibles)
    for possible in possibles:
        newList = current.copy()
        newList.append(possible)
        timelines.append(newList)
    # exit()
    # print(current)
    # if (lastItem == goal):
    #     print("Found a path")
    #     count += 1
    #     continue
    # possibles = getPossibles(lastItem)
