import re

with open('puzzle_input') as file:
    counter = 0
    for line in file:
        match = re.search(r"^(\d*)-(\d*) (\w): (.*)$", line);
        mi = int(match.group(1))
        ma = int(match.group(2))
        ch = match.group(3)
        ss = match.group(4)
        print ("Min: %s, Max: %s, Char: %s, Search String: %s" % (mi, ma, ch, ss))
        count = ss.count(ch)
        print ("Count: %s" % (count))
        if (mi <= count <= ma):
            counter += 1
            print ("Within")
        else:
            print ("Not within")
    print ("Total count: %s" % (counter))
