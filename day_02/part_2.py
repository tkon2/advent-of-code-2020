import re

with open('puzzle_input') as file:
    counter = 0
    for line in file:
        match = re.search(r"^(\d*)-(\d*) (\w): (.*)$", line);
        mi = int(match.group(1))
        ma = int(match.group(2))
        ch = match.group(3)
        ss = match.group(4)
        print ("First char pos: %s, Second char pos: %s, Char: %s, Search String: %s" % (mi, ma, ch, ss))
        print ("Char1: %s, Char2: %s" % (ss[mi - 1], ss[ma - 1]))
        am = (ss[mi - 1] == ch)
        bm = (ss[ma - 1] == ch)

        if (am + bm == 1):
            counter += 1
            print ("Exactly one matching!")
        else:
            print ("Too many or too few, who knows")
    print ("Total count: %s" % (counter))
