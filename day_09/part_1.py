import re
import itertools

data = []
with open('puzzle_input') as file:
    for line in file:
        data.append(line.replace('\n', ''))


def check(needle, haystack):
    combinations = itertools.combinations(haystack, 2)
    for combo in combinations:
        if (int(combo[0]) + int(combo[1]) == int(needle)):
            print("%3s + %3s = %3s" % (combo[0], combo[1], needle))
            return True
        else:
            pass
    return False


def validate(preambleLen=25):
    index = preambleLen
    while(index < len(data)):
        previous = data[index-preambleLen:index]
        ans = check(data[index], previous)
        if (not ans):
            print("No match found for %s" % data[index])
            return data[index]
        index += 1
    return True


print("################ STARTING #################")
validate()
