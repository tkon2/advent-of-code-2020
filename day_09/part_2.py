import re
import itertools

data = []
with open('puzzle_input') as file:
    for line in file:
        data.append(int(line.replace('\n', '')))


def check(needle, haystack):
    combinations = itertools.combinations(haystack, 2)
    for combo in combinations:
        if (int(combo[0]) + int(combo[1]) == int(needle)):
            return True
        else:
            pass
    return False


def validate(preambleLen=25):
    index = preambleLen
    while(index < len(data)):
        previous = data[index-preambleLen:index]
        ans = check(data[index], previous)
        if (not ans):
            return data[index]
        index += 1
    return True


print("################ STARTING #################")
target = validate()

print("Target: %d" % target)

for i, item in enumerate(data):
    for j in range(i + 1, len(data)):
        subset = data[i:j + 1]
        if (sum(subset) == target):
            print("#%d: %d - #%d: %d = %d" %
                  (i, data[i], j, data[j], min(subset) + max(subset)))
            print("Answer: %d" % (min(subset) + max(subset)))
