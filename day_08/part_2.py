import re

data = []
with open('puzzle_input') as file:
    for line in file:
        data.append(line.replace('\n', ''))


def test(arr):
    counter = 0
    pointer = 0
    visited = []
    while(pointer not in visited and pointer < len(arr)):
        visited.append(pointer)
        instruction = arr[pointer]
        match = re.match(r"^(\w{3}) ([-+]\d+)", instruction)
        operation = match.group(1)
        argument = match.group(2)

        if operation == 'nop':
            pointer += 1
        elif operation == 'acc':
            pointer += 1
            counter += int(argument)
        elif operation == 'jmp':
            pointer += int(argument)
    print("Final counter: %d" % counter)
    return pointer >= len(arr)


solution = False
i = 0
while(not solution and i < len(data)):
    item = data[i]
    if (item[0:3] == 'nop'):
        operation = 'jmp' + item[3:]
    elif(item[0:3] == 'jmp'):
        operation = 'nop' + item[3:]
    else:
        i += 1
        continue
    print("array index: %d, old instruction: %s, new instruction: %s" %
          (i, item, operation))
    data[i] = operation
    solution = test(data)
    data[i] = item
    i += 1
