import re

data = []
with open('puzzle_input') as file:
    for line in file:
        if (re.match(r"^\w+ \w+ bags contain no other bags.$", line)):
            print("%s is not even interesting" %
                  re.match(r"^\w+ \w+", line).group(0))
        else:
            data.append(line.replace('\n', ''))


counter = 0
pointer = 0
visited = []
while(pointer not in visited):
    visited.append(pointer)
    instruction = data[pointer]
    print(instruction)
    match = re.match(r"^(\w{3}) ([-+]\d+)", instruction)
    operation = match.group(1)
    argument = match.group(2)

    if operation == 'nop':
        pointer += 1
    elif operation == 'acc':
        pointer += 1
        counter += int(argument)
    elif operation == 'jmp':
        pointer += int(argument)
    print("Pointer: %d, Counter: %d" % (pointer, counter))
