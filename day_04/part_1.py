import re

data = []
passport = ''
with open('puzzle_input') as file:
    for line in file:
        if (line == '\n'):
            data.append(passport)
            passport = ''
        else:
            passport += ' ' + line.replace('\n', '')
    # Don't forget the last one!
    data.append(passport)

passports = []
for passport in data:
    fields = list(filter(None, passport.split(' ')))
    values = {}
    for field in fields:
        match = re.search(r"^(\w{3}):(.*)$", field)
        fieldName = match.group(1)
        fieldValue = match.group(2)
        values[fieldName] = fieldValue
    passports.append(values)

correctPassports = 0
for passport in passports:
    keys = passport.keys()
    print('')
    if (
        'byr' in keys and
        'iyr' in keys and
        'eyr' in keys and
        'hgt' in keys and
        'hcl' in keys and
        'ecl' in keys and
        'pid' in keys
    ):
        correctPassports += 1
        print('## Correct passport')
        print('~~ cid missing, but that\'s fine') if 'cid' not in keys else None
        print(keys)
    else:
        print('## Incorrect passport')
        print('~~ byr missing') if 'byr' not in keys else None
        print('~~ iyr missing') if 'iyr' not in keys else None
        print('~~ eyr missing') if 'eyr' not in keys else None
        print('~~ hgt missing') if 'hgt' not in keys else None
        print('~~ hcl missing') if 'hcl' not in keys else None
        print('~~ ecl missing') if 'ecl' not in keys else None
        print('~~ pid missing') if 'pid' not in keys else None
        print('~~ cid missing, but that\'s fine') if 'cid' not in keys else None
        print(keys)
print(correctPassports)
