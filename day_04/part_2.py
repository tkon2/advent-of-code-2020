import re


def numberBetween(num, low, high):
    number = int(num)
    return low <= number <= high


def validate(passport):
    if not passport['byr'].isdigit():
        print("Error 1")
        return False
    if not passport['iyr'].isdigit():
        print("Error 2")
        return False
    if not passport['eyr'].isdigit():
        print("Error 3")
        return False
    if not re.search(r"^1?[0-9]{2}(cm|in)$", passport['hgt']):
        print("Height %s not in correct format" % (passport['hgt']))
        return False
    if re.search(r"cm$", passport['hgt']):
        if not numberBetween(passport['hgt'][0:-2], 150, 193):
            print("Height %s in cm not between 150 and 193" %
                  (passport['hgt']))
            return False
    elif not numberBetween(passport['hgt'][0:-2], 59, 76):
        print("Height %s in inches not between 59 and 76" % (passport['hgt']))
        return False
    if not re.search(r"^#[0-9a-f]{6}$", passport['hcl']):
        print("Hair colour %s not correct format" % (passport['hcl']))
        return False
    if not re.search(r"^(amb|blu|brn|gry|grn|hzl|oth)$", passport['ecl']):
        print("Eye colour %s not in amb,blu,brn,gry,grn,hzl,oth" %
              (passport['ecl']))
        return False
    if not re.search(r"^[0-9]{9}$", passport['pid']):
        print("Passport ID %s not correct format" % (passport['pid']))
        return False
    if not numberBetween(passport['byr'], 1920, 2002):
        print("Birth year %s not between 1920 and 2002" % (passport['byr']))
        return False
    if not numberBetween(passport['iyr'], 2010, 2020):
        print("Issue year %s not between 1910 and 2020" % (passport['iyr']))
        return False
    if not numberBetween(passport['eyr'], 2020, 2030):
        print("Expiration year %s not between 2020 and 2030" %
              (passport['eyr']))
        return False

    return True


data = []
passport = ''
with open('puzzle_input') as file:
    for line in file:
        if (line == '\n'):
            data.append(passport)
            passport = ''
        else:
            passport += ' ' + line.replace('\n', '')
    # Don't forget the last one!
    data.append(passport)

passports = []
for passport in data:
    fields = list(filter(None, passport.split(' ')))
    values = {}
    for field in fields:
        match = re.search(r"^(\w{3}):(.*)$", field)
        fieldName = match.group(1)
        fieldValue = match.group(2)
        values[fieldName] = fieldValue
    passports.append(values)

correctPassports = 0
for passport in passports:
    keys = passport.keys()
    # print('')
    if (
        'byr' in keys and
        'iyr' in keys and
        'eyr' in keys and
        'hgt' in keys and
        'hcl' in keys and
        'ecl' in keys and
        'pid' in keys and
        validate(passport)
    ):
        correctPassports += 1
        # print('## Correct passport')
        # print('~~ cid missing, but that\'s fine') if 'cid' not in keys else None
        # print(keys)
    else:
        pass
        # print('## Incorrect passport')
        # print('~~ byr missing') if 'byr' not in keys else None
        # print('~~ iyr missing') if 'iyr' not in keys else None
        # print('~~ eyr missing') if 'eyr' not in keys else None
        # print('~~ hgt missing') if 'hgt' not in keys else None
        # print('~~ hcl missing') if 'hcl' not in keys else None
        # print('~~ ecl missing') if 'ecl' not in keys else None
        # print('~~ pid missing') if 'pid' not in keys else None
        # print('~~ cid missing, but that\'s fine') if 'cid' not in keys else None
        # print(keys)
print(correctPassports)
