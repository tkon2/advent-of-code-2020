import re

data = []
with open('puzzle_input') as file:
    for line in file:
        if (re.match(r"^\w+ \w+ bags contain no other bags.$", line)):
            print("%s is not even interesting" %
                  re.match(r"^\w+ \w+", line).group(0))
        else:
            data.append(line.replace('\n', ''))


total = 0
interestingCases = ['shiny gold']

prevTotal = -1
while(prevTotal != total):
    prevTotal = total
    for case in data:
        for ofInterest in interestingCases:
            caseName = re.match(r"^\w+ \w+", case).group(0)
            if (re.match(r"^\w+ \w+ bags contain.*" + ofInterest + r".*$", case) and caseName not in interestingCases):
                print("Found something interesting: %s" % caseName)
                total += 1
                interestingCases.append(caseName)
print("Done!")
print("Total %s" % total)
