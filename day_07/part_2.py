import re

data = []
with open('puzzle_input') as file:
    for line in file:
        if (re.match(r"^\w+ \w+ bags contain no other bags.$", line)):
            print("%s is not even interesting" %
                  re.match(r"^\w+ \w+", line).group(0))
        else:
            data.append(line.replace('\n', ''))


def getCase(needle):
    for straw in data:
        if re.match(r"^" + needle, straw):
            return straw


total = 0
interestingCases = ['1 shiny gold']

while(len(interestingCases) > 0):
    subject = interestingCases.pop()
    subjectMatch = re.match(r"(\d+) (\w+ \w+)", subject)
    multiplier = subjectMatch.group(1)
    subject = subjectMatch.group(2)
    case = getCase(subject)
    if (not case):
        continue
    print(subject)

    match = re.match(
        r".*?((\d+ \w+ \w+ bags?. ?)+)$", case)
    innerCases = match.group(1).replace('.', '').replace(
        ' bags', '').replace(' bag', '').split(', ')

    for case in innerCases:
        caseMatch = re.match(r"(\d+) (\w+ \w+)", case)
        count = caseMatch.group(1)
        caseName = caseMatch.group(2)
        total += int(multiplier) * int(count)
        newMultiplier = int(count) * int(multiplier)
        interestingCases.append("%s %s" % (newMultiplier, caseName))

print("Total %s" % total)
