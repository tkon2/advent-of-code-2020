data = []
passport = ''
with open('puzzle_input') as file:
    for line in file:
        if (line == '\n'):
            data.append(passport)
            passport = ''
        else:
            passport += ' ' + line.replace('\n', '')
    # Don't forget the last one!
    data.append(passport)

total = 0

for item in data:
    str = item.replace(' ', '')
    unique = ''.join(set(str))
    answers = len(unique)
    total += answers

print(total)
