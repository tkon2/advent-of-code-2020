import re
from string import ascii_lowercase

data = []
passport = ''
with open('puzzle_input') as file:
    for line in file:
        if (line == '\n'):
            data.append(passport)
            passport = ''
        else:
            passport += ' ' + line.replace('\n', '')
    # Don't forget the last one!
    data.append(passport)

total = 0

for item in data:
    localTotal = 0
    matches = ''
    for c in ascii_lowercase:
        if re.match(r"^( \w*" + c + r"\w*)+$", item):
            matches = matches + c
            total += 1
            localTotal += 1
        else:
            matches = matches + ' '
    print("%-26s" % matches, end=' ')
    print(" - %2s" % localTotal, end=' -')
    print("%s" % item)

print("\n  Total: %s" % total)
