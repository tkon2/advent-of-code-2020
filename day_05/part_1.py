import re
import math


def decode(p):
    # Decode into a dictionary
    row = 0
    col = 0
    rowDelta = 64
    colDelta = 4
    for c in p:
        if c == 'F':
            rowDelta = math.ceil(rowDelta / 2)
        elif c == 'B':
            row += rowDelta
            rowDelta = math.ceil(rowDelta / 2)
        elif c == 'L':
            colDelta = math.ceil(colDelta / 2)
        elif c == 'R':
            col += colDelta
            colDelta = math.ceil(colDelta / 2)
    return {
        'row': row,
        'col': col,
        'seat': (row * 8) + col,
    }


seats = []
with open('puzzle_input') as file:
    for line in file:
        seats.append(decode(line))

high = 0
for seat in seats:
    val = int(seat['seat'])
    if val > high:
        high = val

print(high)
