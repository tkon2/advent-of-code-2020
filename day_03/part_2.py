import re

data = []
with open('puzzle_input') as file:
    data = file.readlines()

print (data)
def skii(treeMap, stepDown, stepRight):
    trees = 0
    y = 0
    x = 0
    while(y < len(treeMap)):
        x = x % 31 # One less as zero index
        print ("X: %s, Y: %s - %s" % (x,y,treeMap[y][x]))
        if (treeMap[y][x] == '#'):
            trees += 1
        y += stepDown
        x += stepRight
    return trees

run1 = skii(data, 1, 1)
run2 = skii(data, 1, 3)
run3 = skii(data, 1, 5)
run4 = skii(data, 1, 7)
run5 = skii(data, 2, 1)

print (run1 * run2 * run3 * run4 * run5)
